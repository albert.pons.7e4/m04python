# Correccio
# 12/05/2021


def test_abs():
    choice = input("Numero REAL o COMPLEX:")
    while not choice == "REAL" and not choice == "COMPLEX":
        print("ERROR")
        choice = input("Numero REAL o COMPLEX:")
    if choice == "REAL":
        x = float(input_simple())
    else:
        x = input_complex()
    print("Retorna el valor absolut de ", x)
    print("Que es aquest:", abs(x))


def test_all():
    print("Comprova si tots els valors son True / 1")
    x = [1, 2, 3]
    print(all(x))
    x = [1, 0, 3]
    print(all(x))
    x = ['1', '0', '3']
    print(all(x))
    x = []
    print(all(x))
    x = ['']
    print(all(x))
    x = [True, False, True]
    print(all(x))


def test_any():
    print("Comprova si elgun valor es True / 1")
    x = [1, 2, 3]
    print(any(x))
    x = [1, 0, 3]
    print(any(x))
    x = ['1', '0', '3']
    print(all(x))
    x = []
    print(any(x))
    x = ['']
    print(any(x))
    x = [True, False, True]
    print(any(x))


def test_bin():
    print("Representacio binaria d'un sencer:")
    x = int(input_simple())
    print(bin(x))


def test_bool():
    print("Retorna false si l'argument es fals, true si cert:")
    x = 0
    print(bool(x))
    x = 5
    print(bool(x))
    x = []
    print(bool(x))
    x = ['']
    print(bool(x))

def test_chr():
    print("Retorna un caracter que correspon al codi ascii introduit:")
    x = int(input_simple())
    print(chr(x))


def test_divmod():
    print("Retorna el quicoent i la resta de dos numeros")
    x = input_simple()
    y = input_simple()
    print(divmod(x, y))


def test_enumerate():
    print("Enumera una llista: ")
    x = input_list()
    y = enumerate(x)
    print(y)


def test_eval():
    print("Escriu una expressio com a string, saben que x es 10. y es 20 i z es 30.")
    print("eval() evaluara la expressio i en mostrara el resultat")
    """x = 10
    y = 20
    z = 30"""
    print(eval(input_simple()))


def test_filter():
    print("Filtra un array segons una expressio, aqui filtrarem tots els nombres mes petits que 18: ")
    x = input_list()

    def sieve(y):
        if y < 18:
            return False
        else:
            return True
    mesdeu = filter(sieve, x)

    for x in mesdeu:
        print(x)


def test_max():
    print("Retorna el maxim d'un iterable: ")
    x = max(input_list())
    print(x)


def test_min():
    print("Retorna el minim d'un iterable: ")
    x = min(input_list())
    print(x)


def test_pow():
    print("Retorna un numero elevat a un altre numero:")
    print("Primer numero:")
    x = input_simple()
    print("Segon numero:")
    y = input_simple()
    print(pow(x, y))


def test_reverse():
    print("Retorna un iterable amb l'ordre invertit:")
    x = input_list()
    print(x.reverse())


def test_round():
    print("Arrodoneix numero al mes proper: ")
    x = input_simple()
    print(round(x))


def test_sum():
    print("Retorna la suma d'un iterable:")
    x = input_list()
    print(sum(x))


# Input methods:
def input_simple():
    x = input("Introdueix la dada:")
    return x


def input_complex():
    x = complex(input("Introdueix un numero complex (format 'X+Yj'):"))
    return x


def input_list():
    lista = []
    x = int(input("Numero d'elements de la llista:"))
    for i in range(0, x):
        element = input()
        lista.append(element)
    return lista


def input_bool():
    cosa = input("Introdueix TRUE o FALSE nomes:")
    if cosa == "TRUE":
        check = True
        return check
    elif cosa == "FALSE":
        check = False
        return check
    else:
        print("ERROR")
        exit()


def menu():

    while True:
        print("Escull quina funcio executar o surt amb -1:")
        print("[1] abs [2] all [3] any")
        print("[4] bin [5] bool [6] chr")
        print("[7] divmod [8] enumerate [9] eval")
        print("[10] filter [11] max [12] min")
        print("[13] pow [14] reverse [15] round")
        print("[16] sum")
        x = int(input())
        if x == -1:
            exit()
        elif x == 1:
            test_abs()
        elif x == 2:
            test_all()
        elif x == 3:
            test_any()
        elif x == 4:
            test_bin()
        elif x == 5:
            test_bool()
        elif x == 6:
            test_chr()
        elif x == 7:
            test_divmod()
        elif x == 8:
            test_enumerate()
        elif x == 9:
            test_eval()
        elif x == 10:
            test_filter()
        elif x == 11:
            test_max()
        elif x == 12:
            test_min()
        elif x == 13:
            test_pow()
        elif x == 14:
            test_reverse()
        elif x == 15:
            test_round()
        elif x == 16:
            test_sum()
        else:
            print("ERROR: Invalid selection")


def main():
    menu()


# Main
main()
