# Albert Pons Marques
# 03/3/21

# Mètode que afegeix un element al final de la llista
def exemple_append():
    print("\nExemple de l'append\n")
    llista = [1, 2, 3, 4, 5]
    print("Llista original: ", llista)
    llista.append(6)
    print("Afegim element 6 a la llista: ", llista)


def exemple_clear():
    print("\nExemple del clear\n")
    llista = [1, 2, 3, 4, 5]
    print("Llista original: ", llista)
    llista.clear()
    print("Borram la llista: ", llista)


def exemple_copy():
    print("\nExemple del copy\n")
    llista = [1, 2, 3, 4, 5]
    print("Llista original: ", llista)
    x = llista.copy()
    print("Mostrem la llista copiada: ", x)


def exemple_count():
    print("\nExemple del count\n")
    llista = [1, 2, 3, 4, 5, 2, 7, 2]
    print("Llista original: ", llista)
    x = llista.count(2)
    print("Mostrem la quantitat de vegades que surt 2: ", x)


def exemple_extend():
    print("\nExemple de l'extend\n")
    llista1 = [1, 2, 3, 4, 5]
    llista2 = [6, 7, 8, 9, 10]
    print("Llista original i llista nova:  ", llista1 + llista2)
    llista1.extend(llista2)
    print("Mostrem la llista extesa: ", llista1)


def exemple_index():
    print("\nExemple de l'index\n")
    llista = [1, 2, 3, 4, 5]
    print("Llista original: ", llista)
    x = llista.index(4)
    print("Mostra la posició de l'element donat: ", x)


def exemple_insert():
    print("\nExemple de l'insert\n")
    llista = [1, 2, 3, 4, 5]
    print("Llista original: ", llista)
    llista.insert(1, 24)
    print("Mostrem la llista amb l'element insertat a la posició donada: ", llista)


def exemple_pop():
    print("\nExemple del pop\n")
    llista = [1, 2, 3, 4, 5]
    print("Llista original: ", llista)
    llista.pop(4)
    print("Mostrem la llista amb l'element a la posició donada eliminat: ", llista)


def exemple_remove():
    print("\nExemple del remove\n")
    llista = [2, 4, 6, 8, 10]
    print("Llista original: ", llista)
    llista.remove(8)
    print("Mostrem la llista amb l'element donat borrat: ", llista)


def exemple_reverse():
    print("\nExemple del reverse\n")
    llista = [1, 2, 3, 4, 5]
    print("Llista original: ", llista)
    llista.reverse()
    print("Mostrem la llista amb els elements girats de banda: ", llista)


def exemple_sort():
    print("\nExemple del sort\n")
    llista = [5, 3, 19, 4, 1]
    print("Llista original: ", llista)
    llista.sort()
    print("Mostrem la llista ordenada: ", llista)


def exemples_llistes():
    exemple_append()
    exemple_clear()
    exemple_copy()
    exemple_count()
    exemple_extend()
    exemple_index()
    exemple_insert()
    exemple_pop()
    exemple_remove()
    exemple_reverse()
    exemple_sort()


# Main

exemples_llistes()
