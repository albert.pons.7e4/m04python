# Correcció
# 21/04/2021


def ex_items():
    # retorna una llista amb els elements del diccionari. Parell clau-valor
    print("\nItems\n")
    dicc = {'professor': 'Montse', 'A': 'Dual'}
    print("Diccionari")
    print(dicc)
    print(dicc.items())
    print("Items")
    for i,j in dicc.items():
        print(i, j)


def ex_keys():
    # Retorna les claus del diccionari
    print("\nKeys \n")
    dicc = {'professor': 'Montse', 'A': 'Dual'}
    claus = dicc.keys()
    print("Diccionari")
    print(dicc)
    print(claus)
    print("Claus")
    for i in claus:
        print(i)


def ex_values():
    # Retorna una llista amb els valor del diccionari
    print("\nValues \n")
    dicc = {'professor': 'Montse', 'A': 'Dual'}
    print("Diccionari")
    print(dicc)
    print("Valors")
    for i in dicc.values():
        print(i)


def ex_pop_item():
    # Retorna un element aleatori del diccionari
    print("\nPopItem \n")
    dicc = {'professor': 'Montse', 'A': 'Dual', 'B': 'Python'}
    print("Diccionari")
    print(dicc)
    print("Element aleatori")
    item1 = dicc.popitem()
    print("Diccionari:", dicc, "Item:", item1)
    print("Element aleatori")
    print(dicc.popitem())


def ex_clear():
    # Elimina tots els elements del diccionari
    print("\nClear \n")
    dicc = {'professor': 'Montse', 'A': 'Dual'}
    print("Diccionari abans del clear")
    print(dicc)
    print("Diccionari despres del clear")
    dicc.clear()
    print(dicc)


def ex_copy():
    # Copia el contingut del diccionari
    print("\nCopy \n")
    dicc = {'professor': 'Montse', 'A': 'Dual'}
    print("Diccionari original")
    print(dicc)
    new_dicc = dicc.copy()
    print("Nou diccionari copiat")
    print(new_dicc)
    dicc.setdefault('B', 'Python')
    print("Diccionari original")
    print(dicc)
    print("Nou diccionari copiat")
    print(new_dicc)


def ex_pop():
    # dicc.pop('x') Elimnina l'element amb clau x
    print("\nPop \n")
    dicc = {'professor': 'Montse', 'A': 'Dual'}
    print("Diccionari abans del pop")
    print(dicc)
    nom = dicc.pop('professor')
    print("Diccionari despres del pop")
    print(dicc)
    print("Element que hem tret del diccionari")
    print(nom)


def ex_fromkeys():
    # Crea un nou diccionari amb les claus del diccionari indicat, dicc en el nostre cas
    print("\nFromKeys \n")
    dicc = {'professor': 'Montse', 'A': 'Dual'}
    print("Diccionari abans del fromkeys")
    print(dicc)
    new_dicc = dict.fromkeys(dicc)
    new_dicc['professor'] = 'Pepe'
    new_dicc['A'] = 'Python'
    print("Nou diccionari amb les claus anteriors")
    print(new_dicc)


def ex_get():
    # dicc.get('x') Copia els valors del diccionari amb clau x
    print("\nGet \n")
    dicc = {'professor': 'Montse', 'A': 'Dual'}
    print("Diccionari abans del get")
    print(dicc)
    print("Utilitzem dicc.get()")
    print("Professor: ", dicc.get('professor'))
    print("A: ", dicc.get('A'))
    print(dicc.get('B', "No existeix aquesta clau"))
    print(dicc)


def ex_set_default():
    # dicc.setdefault(key[, default_value]) busca al diccionari la clau key i retorna el seu valor.
    # En cas que aquella key no tingui valor retorna None.
    # En cas que la clau no sigui del diccionari, l'afegeix i retorna el valor default_value.
    print("\nSet Default\n")
    dicc = {'professor': 'Montse', 'A': 'Dual'}
    print("Diccionari: ", dicc)
    # Key is not in the dictionary
    dicc.setdefault('salary')
    print("Diccionari despres salary: ", dicc)
    # Key is not in the dictionary
    # default_value is provided
    dicc.setdefault('age', '22')
    print("Diccionari despres de age: ", dicc)
    print('age= ', int(dicc.setdefault('age')))


def ex_update():
    print("\nUpdate\n")
    dicc = {1: "un", 2: "tres"}
    dicc1 = {2: "dos", 3: 'tres'}
    # Actualitzem el valor de la clau 2, ja que el diccionari original ja tenia una clau 2.
    print("Diccionari original: ", dicc)
    dicc.update(dicc1)
    print("Diccionari amb valor clau 2 actualitzada: ", dicc)

    dicc1 = {4: "quatre"}
    # Afegim clau 3 amb valor "tres", ja que no tenia aquesta clau anteriorment
    print("Diccionari actualitzat anteriorment: ", dicc)
    dicc.update(dicc1)
    print("Diccionari amb valor clau 3 actualitzat (afegit): ", dicc)


def menu():
    while True:
        print("*******************\n"
              "       Menu\n"
              "*******************\n"
              "1. Items\n"
              "2. Keys\n"
              "3. Values\n"
              "4. Pop Item\n"
              "5. Clear\n"
              "6. Copy\n"
              "7. Pop\n"
              "8. Fromkeys\n"
              "9. Get\n"
              "10. Set Default\n"
              "11. Update\n"
              "0. Finalitzar\n")
        option = input("Mètode a fer: ")
        if option == '1':
            ex_items()
        elif option == '2':
            ex_keys()
        elif option == '3':
            ex_values()
        elif option == '4':
            ex_pop_item()
        elif option == '5':
            ex_clear()
        elif option == '6':
            ex_copy()
        elif option == '7':
            ex_pop()
        elif option == '8':
            ex_fromkeys()
        elif option == '9':
            ex_get()
        elif option == '10':
            ex_set_default()
        elif option == '11':
            ex_update()
        elif option == '0':
            break
        else:
            print("Opcio incorrecta")


# Main
menu()
