# Albert Pons Marques
# 03/3/21


def exemple_items():
    print("---------- ITEMS ------------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    print("Llista d'items:", dicc.items(), "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_keys():
    print("----------- KEYS ------------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    claus = dicc.keys()
    print("Aquestes son les claus del diccionari:", claus, "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_values():
    print("---------- VALUES -----------")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    claus = dicc.values()
    print("Aquests son els valors del diccionari:", claus, "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_get():
    print("------------ GET ------------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    get_k = dicc.get('Author')
    get_k_x = dicc.get('Height', 'La clau especificada no existeix')
    print("Després d'aplicar get sobre \"Author\" (existeix):", get_k)
    print("Després d'aplicar get sobre \"Height\" (no existeix):", get_k_x, "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_clear():
    print("----------- CLEAR -----------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    dicc.clear()
    print("Després d'aplicar el clear:", dicc, "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_popitem():
    print("-----------POP ITEM ---------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    print("Després de fer un cop popitem:", dicc.popitem())
    print("Després de fer un segon cop popitem:", dicc.popitem(), "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_update():
    print("---------- UPDATE -----------\n")
    dicc1 = {'Author': 'Albert', 'Age': 19}
    dicc2 = {'Hair': 'Black', 'Eyes': 'Blue'}
    print("Diccionari 1:", dicc1)
    print("Diccionari 2:", dicc2)
    print("           - Fem la modificació del diccionari 2 al diccionari 1 - ")
    dicc1.update(dicc2)
    print("Diccionari 1:", dicc1)
    print("Diccionari 2:", dicc2, "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_pop():
    print("---------- POP ------------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    dicc.pop('Author')
    print('Diccionari modificat amb pop: ', dicc)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_fromkeys():
    print("---------- FROMKEYS ------------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    new_dic = dicc.fromkeys(dicc)
    print('Diccionari creat amb fromkeys: ', new_dic)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_setdefault():
    print("---------- SETDEFAULT ------------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    x = dicc.setdefault('Author', 'Albert')
    print('valor seleccionat amb el setdefault: ', x)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_copy():
    print("---------- COPY ------------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    copia = dicc.copy()
    print('valor seleccionat amb el setdefault: ', copia)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def diccionari():
    while True:
        print(" ---- CHOOSE AN OPTION ----")
        print(" |    1.- Item            |\n"
              " |    2.- Keys            |\n"
              " |    3.- Values          |\n"
              " |    4.- Get             |\n"
              " |    5.- Clear           |\n"
              " |    6.- Pop item        |\n"
              " |    7.- Update          |\n"
              " |    8.- Pop             |\n"
              " |    9.- From keys       |\n"
              " |   10.- Set default     |\n"
              " |   11.- Copy            |\n"
              " |                        |\n"
              " |    0.- Exit            |")
        print(" --------------------------")
        option = input()
        print("\n\n\n")
        if option == "1":
            exemple_items()
        elif option == "2":
            exemple_keys()
        elif option == "3":
            exemple_values()
        elif option == "4":
            exemple_get()
        elif option == "5":
            exemple_clear()
        elif option == "6":
            exemple_popitem()
        elif option == "7":
            exemple_update()
        elif option == "8":
            exemple_pop()
        elif option == "9":
            exemple_fromkeys()
        elif option == "10":
            exemple_setdefault()
        # elif opcio == "11":
        #    exemple_copy()
        elif option == "0":
            break


# main
diccionari()
