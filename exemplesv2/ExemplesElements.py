# Albert Pons Marques
# 18/5/2021

import time


def crear_llistes():
    llista = []
    mida = int(input("Introdueix la mida de la llista:"))
    for i in range(mida):
        llista.append(int(input("Element "+str(i+1)+":")))
    return llista


def cmp_llistes(l1, l2):
    if len(l1) != len(l2):
        return False
    for i in range(len(l1)):
        if l1[i] != l2[i]:
            return False
    return True


def primera_funcio():
    print("---------- PRIMER ------------\n")
    print("Aquesta primera funcio determinara si les dues llistes son iguals.")

    llista1 = crear_llistes()
    llista2 = crear_llistes()

    print("Tenim aquestes dues llistes: ")
    print(llista1)
    print(llista2)

    if cmp_llistes(llista1, llista2):
        print("Les llistes son iguals")
    else:
        print("Les llistes no son iguals")

    input("----- ENTER TO CONTINUE -----\n\n\n")


def sumar_fc(matriu):
    sumaf = []
    sumac = []
    for fila in matriu:
        sumaf.append(sum(fila))
    for j in range(len(matriu[0])):
        suma = 0
        for i in range(len(matriu)):
            suma += matriu[i][j]
        sumac.append(suma)
    return sumaf, sumac


def segona_funcio():
    print("----------- SEGON ------------\n")
    matriu = [[1, 2, 3],
              [4, 5, 6],
              [7, 8, 9]]
    print(matriu)
    sumafiles, sumacol = sumar_fc(matriu)

    print("La suma de les files es:", sumafiles)
    print("La suma de les columnes es:", sumacol)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def es_primer(num):
    if num < 1:
        return False
    for i in range(2, num):
        if num % i == 0:
            return False
    return True


def tercera_funcio():
    print("---------- TERCER ------------\n")
    num = int(input("Introdueix un nombre:"))
    primer = es_primer(num)

    if primer:
        print(num, "Es un nombre primer")
    else:
        print(num, "No es un nombre primer")


def generadora_multiples(x):
    y = 1
    while True:
        yield x * y
        y += 1


def funcio_bin():
    print("---------- QUART ------------\n")
    x = int(input("Introdueix un nombre:"))
    for i in generadora_multiples(x):
        print(i)
        time.sleep(.2)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def menu():
    while True:
        print(" ----- TRIA UNA OPCIO -----")
        print(" |    1.- PRIMER          |\n"
              " |    2.- SEGON           |\n"
              " |    3.- TERCER          |\n"
              " |    4.- QUART           |\n"
              " |                        |\n"
              " |    0.- Exit            |")
        print(" --------------------------")
        option = input()
        print("\n\n\n")
        if option == "1":
            primera_funcio()
        elif option == "2":
            segona_funcio()
        elif option == "3":
            tercera_funcio()
        elif option == "4":
            funcio_bin()
        elif option == "0":
            break


# main
menu()
