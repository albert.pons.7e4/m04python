# Alex Navarro Sánchez
# 09/03/2021

# Mètode que afegeix un element al final de la llista
def exemple_append():
    print("Exemple de l'append\n")
    llista = [1, 2, 3]
    print("Llista original:", llista)
    llista.append(4)
    print("Després d'afegir l'element 4 a la llista:", llista, "\n")
    input("Prem enter per a continuar...")


# Mètode que insereix un element en l'index i la llista
def exemple_insert():
    print("-----------------------------")
    print("Exemple de l'insert\n")
    llista = [1, 2, 3]
    print("Llista original:", llista)
    llista.insert(-3, 0)
    print("Després d'afegir l'element 0 a la posició 0 de la llista:", llista, "\n")
    input("Prem enter per a continuar...")


# Mètode que elimina el primer element que apareix a la llista
def exemple_remove():
    print("-----------------------------")
    print("Exemple de remove\n")
    llista = [1, 2, 3]
    print("Llista original:", llista)
    llista.remove(3)
    print("Després d'esborra l'element 3 de la llista:", llista, "\n")
    input("Prem enter per a continuar...")


# Métode que ordena la llista utilitzant comparer com a comparador
def exemple_sort():
    print("-----------------------------")
    print("Exemple de sort\n")
    llista = [3, 1, 2]
    print("Llista original:", llista)
    llista.sort()
    print("Després d'ordenar la llista en ordre descendent:", llista, "\n")
    input("Prem enter per a continuar...")


# Métode que ordena la llista sense modificarla
def exemple_sorted():
    print("-----------------------------")
    print("Exemple de sorted\n")
    llista = [3, 1, 2]
    print("Llista original:", llista)
    llista = sorted(llista)
    print("Després d'ordenar la llista:", llista, "\n")
    input("Prem enter per a continuar...")


# Métode per invertir l'ordre d'una llista
def exemple_reverse():
    print("-----------------------------")
    print("Exemple de reverse\n")
    llista = [1, 2, 3]
    print("Llista original:", llista)
    llista.reverse()
    print("Després d'invertir l'ordre:", llista, "\n")
    input("Prem enter per a continuar...")


# Mètode per eliminar l'element a l'index i el retorna
def exemple_pop():
    print("-----------------------------")
    print("Exemple de pop\n")
    llista = [1, 2, 3]
    print("Llista original:", llista)
    returned_value = llista.pop(1)
    print("Valor retornat:", returned_value)
    print("Després de fer pop a l'element en la posició 1:", llista, "\n")
    input("Prem enter per a continuar...")


# Mètode per afegir tots el elements de la variable a la llista
def exemple_extend():
    print("-----------------------------")
    print("Exemple de extend\n")
    llista = [1, 2, 3]
    print("Llista original:", llista)
    llista.extend([7, 8, 9])
    print("Després de fer l'extend amb els valors 7, 8, 9:", llista, "\n")
    input("Prem enter per a continuar...")


# Mètode que retorna el número de vegades que l'element apareix a la llista
def exemple_count():
    print("-----------------------------")
    print("Exemple de count\n")
    llista = [1, 2, 1, 3, 1]
    print("Llista original:", llista)
    contador = llista.count(1)
    print("Després de fer el count mostrem el resultat:", contador, "\n")
    input("Prem enter per a continuar...")


# Métode per retornar l'índex en que es troba l'element
def exemple_index():
    print("-----------------------------")
    print("Exemple de index\n")
    llista = [1, 2, 3]
    print("Llista original:", llista)
    posicio = llista.index(2)
    print("El número 2 de la llista es troba a la posició:", posicio, "\n")
    input("Prem enter per a continuar...")


def exemples_llistes():
    exemple_append()
    exemple_insert()
    exemple_remove()
    exemple_sort()
    exemple_sorted()
    exemple_reverse()
    exemple_pop()
    exemple_extend()
    exemple_count()
    exemple_index()


# main
exemples_llistes()
