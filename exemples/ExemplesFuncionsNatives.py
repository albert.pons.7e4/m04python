# Albert Pons Marques
# 03/3/21


def funcio_abs():
    print("---------- ABS ------------\n")
    x = 4 * (-5)
    print("Nombre original:", x, "\n")
    y = abs(x)
    print("Nombre amb funcio ABS:", y, "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def funcio_all():
    print("----------- ALL ------------\n")
    primer = [1, 3, 4, 5]
    print("vector 1 amb funció all:", all(primer), "\n")
    segon = [0, False, 5]
    print("vector 2 amb funció all:", all(segon), "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def funcio_any():
    print("----------- ANY ------------\n")
    m = [0, False, 5]
    print("vector 1 amb funció any:", all(m), "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def funcio_bin():
    print("---------- BIN ------------\n")
    x = 20
    print("Nombre original:", x, "\n")
    y = bin(x)
    print("Nombre convertit a binari amb funcio BIN:", y, "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def funcio_bool():
    print("---------- BOOL ------------\n")
    x = 2
    print("Variable 1 aplicant BOOL:", bool(x), "\n")
    y = 0
    print("Variable 2 aplicant BOOL:", bool(y), "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def funcio_chr():
    print("---------- CHR ------------\n")
    x = 209
    print("Nombre original:", x, "\n")
    y = chr(x)
    print("Caracter tradüit a ASCII amb funció chr:", y, "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def funcio_divmod():
    print("---------- DIVMOD ------------\n")
    x = 5
    y = 2
    print("Nombres:", x, y, "\n")
    div = divmod(x, y)
    print("divisió amb DIVMOD:", div, "\n")
    input("----- ENTER TO CONTINUE -----\n\n\n")


def funcio_enumerate():
    print("---------- ENUMERATE ------------\n")
    llista = ['patates', 'bastanagues', 'tomàtigues']
    print("Llista original:", llista)
    listv2 = enumerate(llista)
    print('Llista enumerada amb ENUMERATE: ', listv2)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def funcio_eval():
    print("---------- EVAL ------------\n")
    x = 5
    print("Nombre original:", x)
    print('Nombre sumant amb funció EVAL: ', eval('x + 2'))
    input("----- ENTER TO CONTINUE -----\n\n\n")


def funcio_filter():
    print("---------- FILTER ------------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    x = dicc.setdefault('Author', 'Albert')
    print('valor seleccionat amb el setdefault: ', x)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def exemple_copy():
    print("---------- COPY ------------\n")
    dicc = {'Author': 'Albert', 'Age': 19}
    print("Diccionari original:", dicc)
    copia = dicc.copy()
    print('valor seleccionat amb el setdefault: ', copia)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def diccionari():
    while True:
        print(" ---- CHOOSE AN OPTION ----")
        print(" |    1.- abs             |\n"
              " |    2.- all             |\n"
              " |    3.- any             |\n"
              " |    4.- bin             |\n"
              " |    5.- bool            |\n"
              " |    6.- chr             |\n"
              " |    7.- divmod          |\n"
              " |    8.- enumerate       |\n"
              " |    9.-                 |\n"
              " |   10.-                 |\n"
              " |   11.-                 |\n"
              " |                        |\n"
              " |    0.- Exit            |")
        print(" --------------------------")
        option = input()
        print("\n\n\n")
        if option == "1":
            funcio_abs()
        elif option == "2":
            funcio_all()
        elif option == "3":
            funcio_any()
        elif option == "4":
            funcio_bin()
        elif option == "5":
            funcio_bool()
        elif option == "6":
            funcio_chr()
        elif option == "7":
            funcio_divmod()
        elif option == "8":
            funcio_enumerate()
        elif option == "9":
            funcio_eval()
        # elif option == "10":
        #   exemple_setdefault()
        # elif opcio == "11":
        #    exemple_copy()
        # elif option == "0":
        #    break


# main
diccionari()
