# Albert Pons Marques
# 18/5/2021

import time


def primera_funcio():
    print("---------- PRIMER ------------\n")
    print("Aquesta primera funcio determinara si les dues llistes son iguals.")
    print("Introdueix les dades de la primera llista")
    llista1 = [input(), input(), input(), input(), input()]
    print("Introdueix les dades de la segona llista")
    llista2 = [input(), input(), input(), input(), input()]

    # Les ordenare
    llista1.sort()
    llista2.sort()
    print("Tenim aquestes dues llistes: ")
    print(llista1)
    print(llista2)

    if llista1 == llista2:
        print("Les llistes son iguals")
    else:
        print("Les llistes no son iguals")

    input("----- ENTER TO CONTINUE -----\n\n\n")


def segona_funcio():
    print("----------- SEGON ------------\n")
    matriu = [[input(), input(), input()],
              [input(), input(), input()],
              [input(), input(), input()]]

    tupla = ()

    for x in range(6):
        for y in range(6):
            tupla.__add__(tupla[1])

    input("----- ENTER TO CONTINUE -----\n\n\n")


def es_primer(num):
    if num < 1:
        return False
    elif num == 2:
        return True
    else:
        for i in range(2, num):
            if num % i == 0:
                return False
        return True


def tercera_funcio():
    print("---------- TERCER ------------\n")
    num = int(input("Introdueix un nombre:"))
    primer = es_primer(num)

    if primer is True:
        print(num, "Es un nombre primer")
    else:
        print(num, "No es un nombre primer")


def funcio_bin():
    print("---------- QUART ------------\n")
    x = int(input("Introdueix un nombre:"))
    y = 0
    i = 0
    llista = []
    while i <= 2:
        llista.append(x * y)
        y += 1
        print(llista[y - 1])
        time.sleep(.2)
    input("----- ENTER TO CONTINUE -----\n\n\n")


def menu():
    while True:
        print(" ----- TRIA UNA OPCIO -----")
        print(" |    1.- PRIMER          |\n"
              " |    2.- SEGON           |\n"
              " |    3.- TERCER          |\n"
              " |    4.- QUART           |\n"
              " |                        |\n"
              " |    0.- Exit            |")
        print(" --------------------------")
        option = input()
        print("\n\n\n")
        if option == "1":
            primera_funcio()
            break
        elif option == "2":
            segona_funcio()
            break
        elif option == "3":
            tercera_funcio()
            break
        elif option == "4":
            funcio_bin()
            break
        elif option == "0":
            break


# main
menu()
